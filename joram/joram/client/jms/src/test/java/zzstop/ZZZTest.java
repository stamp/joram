package zzstop;

import java.io.File;

import org.junit.jupiter.api.Test;
import org.ow2.joram.testutils.AgentManager;

import junit.framework.TestCase;

public class ZZZTest extends TestCase {

	@Test
	public void testStopAgent() {
		System.setProperty("felix.dir", "../../../ship/bin");
		try {
			AgentManager.stopAgentServer();
			
			// cleanup !
			deleteDirectory(new File("s0"));
			for (File f : (new File(".")).listFiles()) {
			    if (f.getName().startsWith("server.log.")) {
			        f.delete();
			    }
			}
		} catch(Exception e) {
		}
	}

	private boolean deleteDirectory(File dir) {
	    File[] files = dir.listFiles();
	    if (files != null) {
	        for (File file : files) {
	            deleteDirectory(file);
	        }
	    }
	    return dir.delete();
	}
}
