package jms.conform.message;

public class AmplMessageTypeTest extends jms.framework.PTPTestCase {
    @org.junit.jupiter.api.Test
    public void testStreamMessage_2() {
        try {
            javax.jms.StreamMessage message = senderSession.createStreamMessage();
            message.writeString("pi");
            message.writeDouble(3.14159);
            sender.send(message);
            javax.jms.Message m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue("The message should be an instance of StreamMessage.\n", (m instanceof javax.jms.StreamMessage));
            javax.jms.StreamMessage msg = ((javax.jms.StreamMessage) (m));
            junit.framework.TestCase.assertEquals("pi", msg.readString());
            junit.framework.TestCase.assertEquals(3.14159, msg.readDouble(), 0);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testStreamMessage_1() {
        try {
            javax.jms.StreamMessage message = senderSession.createStreamMessage();
            sender.send(message);
            javax.jms.Message msg = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue("The message should be an instance of StreamMessage.\n", (msg instanceof javax.jms.StreamMessage));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testMapMessageConversion() {
        try {
            javax.jms.MapMessage message = senderSession.createMapMessage();
            message.setDouble("pi", 3.14159);
            sender.send(message);
            javax.jms.Message m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue("The message should be an instance of MapMessage.\n", (m instanceof javax.jms.MapMessage));
            javax.jms.MapMessage msg = ((javax.jms.MapMessage) (m));
            junit.framework.TestCase.assertTrue(((msg.getObject("pi")) instanceof java.lang.Double));
            junit.framework.TestCase.assertEquals(3.14159, ((java.lang.Double) (msg.getObject("pi"))).doubleValue(), 0);
            junit.framework.TestCase.assertEquals(3.14159, msg.getDouble("pi"), 0);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testNullInSetMethodsForMapMessage() {
        try {
            javax.jms.MapMessage message = senderSession.createMapMessage();
            message.setBoolean(null, true);
            junit.framework.TestCase.fail("Should throw an IllegalArgumentException");
        } catch (java.lang.IllegalArgumentException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw an IllegalArgumentException, not a" + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testEmptyStringInSetMethodsForMapMessage() {
        try {
            javax.jms.MapMessage message = senderSession.createMapMessage();
            message.setBoolean("", true);
            junit.framework.TestCase.fail("Should throw an IllegalArgumentException");
        } catch (java.lang.IllegalArgumentException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw an IllegalArgumentException, not a" + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testgetMapNames() {
        try {
            javax.jms.MapMessage message = senderSession.createMapMessage();
            java.util.Enumeration e = message.getMapNames();
            junit.framework.TestCase.assertTrue("No map yet defined.\n", (!(e.hasMoreElements())));
            message.setDouble("pi", 3.14159);
            e = message.getMapNames();
            junit.framework.TestCase.assertEquals("pi", ((java.lang.String) (e.nextElement())));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testMapMessage_2() {
        try {
            javax.jms.MapMessage message = senderSession.createMapMessage();
            message.setString("name", "pi");
            message.setDouble("value", 3.14159);
            sender.send(message);
            javax.jms.Message m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue("The message should be an instance of MapMessage.\n", (m instanceof javax.jms.MapMessage));
            javax.jms.MapMessage msg = ((javax.jms.MapMessage) (m));
            junit.framework.TestCase.assertEquals("pi", msg.getString("name"));
            junit.framework.TestCase.assertEquals(3.14159, msg.getDouble("value"), 0);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testMapMessage_1() {
        try {
            javax.jms.MapMessage message = senderSession.createMapMessage();
            sender.send(message);
            javax.jms.Message msg = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue("The message should be an instance of MapMessage.\n", (msg instanceof javax.jms.MapMessage));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testObjectMessage_2() {
        try {
            java.util.Vector vector = new java.util.Vector();
            vector.add("pi");
            vector.add(new java.lang.Double(3.14159));
            javax.jms.ObjectMessage message = senderSession.createObjectMessage();
            message.setObject(vector);
            sender.send(message);
            javax.jms.Message m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue("The message should be an instance of ObjectMessage.\n", (m instanceof javax.jms.ObjectMessage));
            javax.jms.ObjectMessage msg = ((javax.jms.ObjectMessage) (m));
            junit.framework.TestCase.assertEquals(vector, msg.getObject());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testObjectMessage_1() {
        try {
            javax.jms.ObjectMessage message = senderSession.createObjectMessage();
            sender.send(message);
            javax.jms.Message msg = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue("The message should be an instance of ObjectMessage.\n", (msg instanceof javax.jms.ObjectMessage));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testBytesMessage_2() {
        try {
            byte[] bytes = new java.lang.String("pi").getBytes();
            javax.jms.BytesMessage message = senderSession.createBytesMessage();
            message.writeBytes(bytes);
            message.writeDouble(3.14159);
            sender.send(message);
            javax.jms.Message m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue("The message should be an instance of BytesMessage.\n", (m instanceof javax.jms.BytesMessage));
            javax.jms.BytesMessage msg = ((javax.jms.BytesMessage) (m));
            byte[] receivedBytes = new byte[bytes.length];
            msg.readBytes(receivedBytes);
            junit.framework.TestCase.assertEquals(new java.lang.String(bytes), new java.lang.String(receivedBytes));
            junit.framework.TestCase.assertEquals(3.14159, msg.readDouble(), 0);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testBytesMessage_1() {
        try {
            javax.jms.BytesMessage message = senderSession.createBytesMessage();
            sender.send(message);
            javax.jms.Message msg = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue("The message should be an instance of BytesMessage.\n", (msg instanceof javax.jms.BytesMessage));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testTextMessage_2() {
        try {
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setText("testTextMessage_2");
            sender.send(message);
            javax.jms.Message m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue("The message should be an instance of TextMessage.\n", (m instanceof javax.jms.TextMessage));
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (m));
            junit.framework.TestCase.assertEquals("testTextMessage_2", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testTextMessage_1() {
        try {
            javax.jms.TextMessage message = senderSession.createTextMessage();
            sender.send(message);
            javax.jms.Message msg = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue("The message should be an instance of TextMessage.\n", (msg instanceof javax.jms.TextMessage));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }
}
