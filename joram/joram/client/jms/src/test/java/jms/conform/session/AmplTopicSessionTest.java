package jms.conform.session;

public class AmplTopicSessionTest extends jms.framework.PubSubTestCase {
    @org.junit.jupiter.api.Test
    public void testRollbackReceivedMessage() {
        try {
            publisherConnection.stop();
            publisherSession = publisherConnection.createTopicSession(true, 0);
            junit.framework.TestCase.assertEquals(true, publisherSession.getTransacted());
            publisher = publisherSession.createPublisher(publisherTopic);
            publisherConnection.start();
            subscriberConnection.stop();
            subscriberSession = subscriberConnection.createTopicSession(true, 0);
            junit.framework.TestCase.assertEquals(true, subscriberSession.getTransacted());
            subscriber = subscriberSession.createSubscriber(subscriberTopic);
            subscriberConnection.start();
            javax.jms.TextMessage message = publisherSession.createTextMessage();
            message.setText("testRollbackReceivedMessage");
            publisher.publish(message);
            publisherSession.commit();
            javax.jms.Message msg1 = subscriber.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue("no message received", (msg1 != null));
            junit.framework.TestCase.assertTrue((msg1 instanceof javax.jms.TextMessage));
            junit.framework.TestCase.assertEquals("testRollbackReceivedMessage", ((javax.jms.TextMessage) (msg1)).getText());
            subscriberSession.rollback();
            javax.jms.Message msg2 = subscriber.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue("no message received after rollbacking subscriber session.", (msg2 != null));
            junit.framework.TestCase.assertTrue((msg2 instanceof javax.jms.TextMessage));
            junit.framework.TestCase.assertEquals("testRollbackReceivedMessage", ((javax.jms.TextMessage) (msg2)).getText());
            subscriberSession.commit();
        } catch (java.lang.Exception e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testDurableSubscriber() {
        try {
            subscriber = subscriberSession.createDurableSubscriber(subscriberTopic, "testTopic");
            subscriberConnection.close();
            subscriberConnection = null;
            javax.jms.TextMessage message = publisherSession.createTextMessage();
            message.setText("test");
            publisher.publish(message);
            subscriberConnection = subscriberTCF.createTopicConnection();
            if ((subscriberConnection.getClientID()) == null) {
                subscriberConnection.setClientID("subscriberConnection");
            }
            subscriberSession = subscriberConnection.createTopicSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
            subscriber = subscriberSession.createDurableSubscriber(subscriberTopic, "testTopic");
            subscriberConnection.start();
            javax.jms.TextMessage m = ((javax.jms.TextMessage) (subscriber.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertTrue((m != null));
            junit.framework.TestCase.assertEquals("test", m.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testDurableConsumer() {
        javax.jms.MessageConsumer subscriber = null;
        try {
            subscriber = subscriberSession.createDurableConsumer(subscriberTopic, "testTopic");
            subscriberConnection.close();
            subscriberConnection = null;
            javax.jms.TextMessage message = publisherSession.createTextMessage();
            message.setText("test");
            publisher.publish(message);
            subscriberConnection = subscriberTCF.createTopicConnection();
            if ((subscriberConnection.getClientID()) == null) {
                subscriberConnection.setClientID("subscriberConnection");
            }
            subscriberSession = subscriberConnection.createTopicSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
            subscriber = subscriberSession.createDurableConsumer(subscriberTopic, "testTopic");
            subscriberConnection.start();
            javax.jms.TextMessage m = ((javax.jms.TextMessage) (subscriber.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertTrue((m != null));
            junit.framework.TestCase.assertEquals("test", m.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testUnsubscribe() {
        try {
            subscriber = subscriberSession.createDurableSubscriber(subscriberTopic, "topic");
            subscriber.close();
            subscriberSession.unsubscribe("topic");
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testCreateDurableSubscriber_2() {
        try {
            subscriberSession.createDurableSubscriber(subscriberTopic, "topic", "definitely not a message selector!", true);
            junit.framework.TestCase.fail("Should throw a javax.jms.InvalidSelectorException.\n");
        } catch (javax.jms.InvalidSelectorException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.InvalidSelectorException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testCreateDurableSubscriber_1() {
        try {
            subscriberSession.createDurableSubscriber(((javax.jms.Topic) (null)), "topic");
            junit.framework.TestCase.fail("Should throw a javax.jms.InvalidDestinationException.\n");
        } catch (javax.jms.InvalidDestinationException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.InvalidDestinationException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testCreateSubscriber_2() {
        try {
            subscriberSession.createSubscriber(subscriberTopic, "definitely not a message selector!", true);
            junit.framework.TestCase.fail("Should throw a javax.jms.InvalidSelectorException.\n");
        } catch (javax.jms.InvalidSelectorException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.InvalidSelectorException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testCreateSubscriber_1() {
        try {
            subscriberSession.createSubscriber(((javax.jms.Topic) (null)));
            junit.framework.TestCase.fail("Should throw a javax.jms.InvalidDestinationException.\n");
        } catch (javax.jms.InvalidDestinationException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.InvalidDestinationException, not a " + e));
        }
    }
}

