package jms.conform.selector;

public class AmplSelectorSyntaxTest extends jms.framework.PTPTestCase {
    @org.junit.jupiter.api.Test
    public void testValidIdentifiersStart() {
        java.lang.String identifier = null;
        try {
            identifier = "_correct";
            junit.framework.TestCase.assertTrue((identifier + " starts with an invalid Java identifier start character"), java.lang.Character.isJavaIdentifierStart(identifier.charAt(0)));
            receiver = receiverSession.createReceiver(receiverQueue, (identifier + " IS NULL"));
            identifier = "$correct";
            junit.framework.TestCase.assertTrue((identifier + " starts with an invalid Java identifier start character"), java.lang.Character.isJavaIdentifierStart(identifier.charAt(0)));
            receiver = receiverSession.createReceiver(receiverQueue, (identifier + " IS NULL"));
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(((identifier + " is a correct identifier. \n") + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testInvalidIdentifiersStart() {
        java.lang.String identifier = null;
        try {
            identifier = "1uncorrect";
            junit.framework.TestCase.assertTrue((identifier + " starts with an invalid Java identifier start character"), (!(java.lang.Character.isJavaIdentifierStart(identifier.charAt(0)))));
            receiver = receiverSession.createReceiver(receiverQueue, (identifier + " IS NULL"));
            junit.framework.TestCase.fail((identifier + " starts with an invalid Java identifier start character"));
        } catch (javax.jms.JMSException e) {
        }
        try {
            identifier = "%uncorrect";
            junit.framework.TestCase.assertTrue((identifier + " starts with an invalid Java identifier start character"), (!(java.lang.Character.isJavaIdentifierStart(identifier.charAt(0)))));
            receiver = receiverSession.createReceiver(receiverQueue, (identifier + " IS NULL"));
            junit.framework.TestCase.fail((identifier + " starts with an invalid Java identifier start character"));
        } catch (javax.jms.JMSException e) {
        }
    }

    @org.junit.jupiter.api.Test
    public void testEmptyStringAsSelector() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "");
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testIdentifierNULL() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "NULL = ZERO");
            junit.framework.TestCase.fail("NULL is not a valid identifier");
        } catch (javax.jms.InvalidSelectorException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testIdentifierTRUE() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "TRUE > 0");
            junit.framework.TestCase.fail("TRUE is not a valid identifier");
        } catch (javax.jms.JMSException e) {
        }
    }

    @org.junit.jupiter.api.Test
    public void testIdentifierFALSE() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "FALSE > 0");
            junit.framework.TestCase.fail("FALSE is not a valid identifier");
        } catch (javax.jms.JMSException e) {
        }
    }

    @org.junit.jupiter.api.Test
    public void testIdentifierNOT() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "NOT > 0");
            junit.framework.TestCase.fail("NOT is not a valid identifier");
        } catch (javax.jms.JMSException e) {
        }
    }

    @org.junit.jupiter.api.Test
    public void testIdentifierAND() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "AND > 0");
            junit.framework.TestCase.fail("AND is not a valid identifier");
        } catch (javax.jms.JMSException e) {
        }
    }

    @org.junit.jupiter.api.Test
    public void testIdentifierOR() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "OR > 0");
            junit.framework.TestCase.fail("OR is not a valid identifier");
        } catch (javax.jms.JMSException e) {
        }
    }

    @org.junit.jupiter.api.Test
    public void testIdentifierBETWEEN() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "BETWEEN > 0");
            junit.framework.TestCase.fail("BETWEEN is not a valid identifier");
        } catch (javax.jms.JMSException e) {
        }
    }

    @org.junit.jupiter.api.Test
    public void testIdentifierLIKE() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "LIKE > 0");
            junit.framework.TestCase.fail("LIKE is not a valid identifier");
        } catch (javax.jms.JMSException e) {
        }
    }

    @org.junit.jupiter.api.Test
    public void testIdentifierIN() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "IN > 0");
            junit.framework.TestCase.fail("IN is not a valid identifier");
        } catch (javax.jms.JMSException e) {
        }
    }

    @org.junit.jupiter.api.Test
    public void testIdentifierIS() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "IS > 0");
            junit.framework.TestCase.fail("IS is not a valid identifier");
        } catch (javax.jms.JMSException e) {
        }
    }

    @org.junit.jupiter.api.Test
    public void testIdentifierESCAPE() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "ESCAPE > 0");
            junit.framework.TestCase.fail("ESCAPE is not a valid identifier");
        } catch (javax.jms.JMSException e) {
        }
    }

    @org.junit.jupiter.api.Test
    public void testNull() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "prop_name IS NULL");
            receiver = receiverSession.createReceiver(receiverQueue, "prop_name IS NOT NULL");
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testLike() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "phone LIKE '12%3'");
            receiver = receiverSession.createReceiver(receiverQueue, "word LIKE 'l_se'");
            receiver = receiverSession.createReceiver(receiverQueue, "underscored LIKE \'\\_%\' ESCAPE \'\\\'");
            receiver = receiverSession.createReceiver(receiverQueue, "phone NOT LIKE '12%3'");
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testIn() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "Country IN ('UK', 'US', 'France')");
            receiver = receiverSession.createReceiver(receiverQueue, "Country NOT IN ('UK', 'US', 'France')");
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testBetween() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "age BETWEEN 15 and 19");
            receiver = receiverSession.createReceiver(receiverQueue, "age NOT BETWEEN 15 and 19");
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testApproximateNumericLiteral() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "average = +6.2");
            receiver = receiverSession.createReceiver(receiverQueue, "average = -95.7");
            receiver = receiverSession.createReceiver(receiverQueue, "average = 7.");
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testExactNumericLiteral() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "average = +62");
            receiver = receiverSession.createReceiver(receiverQueue, "max = -957");
            receiver = receiverSession.createReceiver(receiverQueue, "max = 57");
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testZero() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "max = 0");
            receiver = receiverSession.createReceiver(receiverQueue, "max = 0.0");
            receiver = receiverSession.createReceiver(receiverQueue, "max = 0.");
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testString() {
        try {
            receiver = receiverSession.createReceiver(receiverQueue, "string = 'literal'");
            receiver = receiverSession.createReceiver(receiverQueue, "string = 'literal''s'");
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }
}

