package jms.conform.connection;

public class AmplTopicConnectionTest extends jms.framework.PubSubTestCase {
    @org.junit.jupiter.api.Test
    public void testSetClientID_1() {
        try {
            subscriberConnection.close();
            subscriberConnection = null;
            subscriberConnection = subscriberTCF.createTopicConnection();
            if ((subscriberConnection.getClientID()) == null) {
                subscriberConnection.setClientID("testSetClientID_1");
                junit.framework.TestCase.assertEquals("testSetClientID_1", subscriberConnection.getClientID());
            }
            junit.framework.TestCase.assertTrue(((subscriberConnection.getClientID()) != null));
            subscriberConnection.setClientID("another client ID");
            junit.framework.TestCase.fail("Should raise a javax.jms.IllegalStateException");
        } catch (javax.jms.IllegalStateException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should raise a javax.jms.IllegalStateException, not a " + e));
        } catch (java.lang.IllegalStateException e) {
            junit.framework.TestCase.fail("Should raise a javax.jms.IllegalStateException, not a java.lang.IllegalStateException");
        }
    }

    @org.junit.jupiter.api.Test
    public void testSetClientID_2() {
        try {
            subscriberConnection.close();
            subscriberConnection = null;
            subscriberConnection = subscriberTCF.createTopicConnection();
            if ((subscriberConnection.getClientID()) != null) {
                return;
            }
            subscriberConnection.start();
            subscriberConnection.setClientID("testSetClientID_2");
            junit.framework.TestCase.fail("Should throw a javax.jms.IllegalStateException");
        } catch (javax.jms.IllegalStateException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should raise a javax.jms.IllegalStateException, not a " + e));
        } catch (java.lang.IllegalStateException e) {
            junit.framework.TestCase.fail("Should raise a javax.jms.IllegalStateException, not a java.lang.IllegalStateException");
        }
    }

    @org.junit.jupiter.api.Test
    public void testSetClientID_3() {
        try {
            subscriberConnection.close();
            subscriberConnection = null;
            subscriberConnection = subscriberTCF.createTopicConnection();
            if ((subscriberConnection.getClientID()) != null) {
                return;
            }
            subscriberConnection.setClientID("testSetClientID_3");
            junit.framework.TestCase.assertEquals("testSetClientID_3", subscriberConnection.getClientID());
            javax.jms.TopicConnection connection_2 = subscriberTCF.createTopicConnection();
            junit.framework.TestCase.assertTrue(((connection_2.getClientID()) == null));
            connection_2.setClientID("testSetClientID_3");
            junit.framework.TestCase.fail("Should throw a javax.jms.InvalidClientIDException");
        } catch (javax.jms.InvalidClientIDException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }
}

