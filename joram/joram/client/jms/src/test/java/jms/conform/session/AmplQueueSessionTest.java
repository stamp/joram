package jms.conform.session;

public class AmplQueueSessionTest extends jms.framework.PTPTestCase {
    @org.junit.jupiter.api.Test
    public void testRollbackRececeivedMessage() {
        try {
            senderConnection.stop();
            senderSession = senderConnection.createQueueSession(true, 0);
            junit.framework.TestCase.assertEquals(true, senderSession.getTransacted());
            sender = senderSession.createSender(senderQueue);
            senderConnection.start();
            receiverConnection.stop();
            receiverSession = receiverConnection.createQueueSession(true, 0);
            junit.framework.TestCase.assertEquals(true, receiverSession.getTransacted());
            receiver = receiverSession.createReceiver(receiverQueue);
            receiverConnection.start();
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setText("testRollbackRececeivedMessage");
            sender.send(message);
            senderSession.commit();
            javax.jms.Message m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue((m != null));
            junit.framework.TestCase.assertTrue((m instanceof javax.jms.TextMessage));
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (m));
            junit.framework.TestCase.assertEquals("testRollbackRececeivedMessage", msg.getText());
            junit.framework.TestCase.assertEquals(false, msg.getJMSRedelivered());
            receiverSession.rollback();
            m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue((m != null));
            junit.framework.TestCase.assertTrue((m instanceof javax.jms.TextMessage));
            msg = ((javax.jms.TextMessage) (m));
            junit.framework.TestCase.assertEquals("testRollbackRececeivedMessage", msg.getText());
            junit.framework.TestCase.assertEquals(true, msg.getJMSRedelivered());
        } catch (java.lang.Exception e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testCreateBrowser_2() {
        try {
            javax.jms.QueueBrowser browser = senderSession.createBrowser(senderQueue, "definitely not a message selector!");
            junit.framework.TestCase.fail("Should throw a javax.jms.InvalidSelectorException.\n");
        } catch (javax.jms.InvalidSelectorException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.InvalidSelectorException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testCreateBrowser_1() {
        try {
            javax.jms.QueueBrowser browser = senderSession.createBrowser(((javax.jms.Queue) (null)));
            junit.framework.TestCase.fail("Should throw a javax.jms.InvalidDestinationException.\n");
        } catch (javax.jms.InvalidDestinationException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.InvalidDestinationException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testCreateReceiver_2() {
        try {
            receiver = senderSession.createReceiver(senderQueue, "definitely not a message selector!");
            junit.framework.TestCase.fail("Should throw a javax.jms.InvalidSelectorException.\n");
        } catch (javax.jms.InvalidSelectorException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.InvalidSelectorException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testCreateReceiver_1() {
        try {
            receiver = senderSession.createReceiver(((javax.jms.Queue) (null)));
            junit.framework.TestCase.fail("Should throw a javax.jms.InvalidDestinationException.\n");
        } catch (javax.jms.InvalidDestinationException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.InvalidDestinationException, not a " + e));
        }
    }
}

