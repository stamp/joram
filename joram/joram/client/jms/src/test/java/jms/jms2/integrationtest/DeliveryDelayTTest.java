/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C)  2016 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s):
 * Contributor(s): 
 */
package jms.jms2.integrationtest;

import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.junit.jupiter.api.Test;
import org.objectweb.joram.client.jms.Topic;
import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;
import org.ow2.joram.testutils.AgentManager;

import junit.framework.TestCase;

/**
 * Test the delivery delay.
 */
public class DeliveryDelayTTest extends TestCase implements javax.jms.MessageListener {
  
  JMSContext context;
  JMSConsumer consumer;
  
  public void setUp() throws Exception {
	  cf = TcpConnectionFactory.create("localhost", 16010);
	  AdminModule.connect(cf, "root", "root");   
	  User.create("anonymous", "anonymous", 0);
	  dest = Topic.create("DeliveryDelayTTest");
	  dest.setFreeReading();
	  dest.setFreeWriting();
	  AdminModule.disconnect();
  }
  
  @Test
  public void testDeliveryDelay() throws Exception {

	  deliverMessage();

	  AgentManager.restartAgentServer();

	  System.out.println("Server restarted.");
	  deliverMessage();

	  assertTrue("Should receive 2 messages: " + nbrecv, (nbrecv == 2));
  }
  
  ConnectionFactory cf = null;
  Topic dest = null;

  long send = 0L;
  long recv = 0L;

  void deliverMessage() throws InterruptedException {
    context = cf.createContext(JMSContext.AUTO_ACKNOWLEDGE);
    consumer = context.createConsumer(dest);
    consumer.setMessageListener(this);
    context.start();

    JMSContext prodCtx = cf.createContext();
    JMSProducer producer = prodCtx.createProducer();
    producer.setDeliveryMode(DeliveryMode.PERSISTENT);
    producer.setDeliveryDelay(5000L);
    TextMessage msg = prodCtx.createTextMessage("test redeliveryTime");
    send = System.currentTimeMillis();
    producer.send(dest, msg);

    // Wait to receive the message.
    Thread.sleep(6000);

    assertTrue("The rollback or recover message not received after the redelivery delay", (recv - send) > 5000);

    context.close();
    prodCtx.close();
  }
  
  int nbrecv = 0;
  
  public void onMessage(Message message) {
    try {
      nbrecv += 1;
      System.out.println("#" + nbrecv + " - " + message.getJMSMessageID() + ", JMSRedelivered = " + message.getJMSRedelivered() + 
                         ", JMSTimestamp = " + message.getJMSTimestamp());
      System.out.println(System.currentTimeMillis() + ": message received deliveryTime = " + message.getJMSDeliveryTime());
    } catch (JMSException e) {
      e.printStackTrace();
    }
    recv = System.currentTimeMillis();
  }

}
