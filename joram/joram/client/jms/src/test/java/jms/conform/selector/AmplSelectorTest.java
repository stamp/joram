package jms.conform.selector;

public class AmplSelectorTest extends jms.framework.PTPTestCase {
    @org.junit.jupiter.api.Test
    public void testEmptyStringAsSelector() {
        try {
            receiverConnection.stop();
            receiver = receiverSession.createReceiver(receiverQueue, "");
            receiverConnection.start();
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setText("testEmptyStringAsSelector");
            sender.send(message);
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertTrue("No message was received", (msg != null));
            junit.framework.TestCase.assertEquals("testEmptyStringAsSelector", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testStringLiterals() {
        try {
            receiverConnection.stop();
            receiver = receiverSession.createReceiver(receiverQueue, "string = 'literal''s'");
            receiverConnection.start();
            javax.jms.TextMessage dummyMessage = senderSession.createTextMessage();
            dummyMessage.setStringProperty("string", "literal");
            dummyMessage.setText("testStringLiterals:1");
            sender.send(dummyMessage);
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setStringProperty("string", "literal's");
            message.setText("testStringLiterals:2");
            sender.send(message);
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertTrue("No message was received", (msg != null));
            junit.framework.TestCase.assertEquals("testStringLiterals:2", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testJMSDeliveryModeInSelector() {
        try {
            receiverConnection.stop();
            receiver = receiverSession.createReceiver(receiverQueue, "JMSDeliveryMode = 'PERSISTENT'");
            receiverConnection.start();
            javax.jms.TextMessage dummyMessage = senderSession.createTextMessage();
            dummyMessage.setText("testJMSDeliveryModeInSelector:1");
            sender.send(dummyMessage, javax.jms.DeliveryMode.NON_PERSISTENT, sender.getPriority(), sender.getTimeToLive());
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setText("testJMSDeliveryModeInSelector:2");
            sender.send(message, javax.jms.DeliveryMode.PERSISTENT, sender.getPriority(), sender.getTimeToLive());
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertTrue("No message was received", (msg != null));
            junit.framework.TestCase.assertEquals(javax.jms.DeliveryMode.PERSISTENT, msg.getJMSDeliveryMode());
            junit.framework.TestCase.assertEquals("testJMSDeliveryModeInSelector:2", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testIdentifierConversion() {
        try {
            receiverConnection.stop();
            receiver = receiverSession.createReceiver(receiverQueue, "NumberOfOrders > 1");
            receiverConnection.start();
            javax.jms.TextMessage dummyMessage = senderSession.createTextMessage();
            dummyMessage.setStringProperty("NumberOfOrders", "2");
            dummyMessage.setText("testIdentifierConversion:1");
            sender.send(dummyMessage);
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setIntProperty("NumberOfOrders", 2);
            message.setText("testIdentifierConversion:2");
            sender.send(message);
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertEquals("testIdentifierConversion:2", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testSelectorExampleFromSpecs() {
        try {
            receiverConnection.stop();
            receiver = receiverSession.createReceiver(receiverQueue, "JMSType = 'car' AND color = 'blue' AND weight > 2500");
            receiverConnection.start();
            javax.jms.TextMessage dummyMessage = senderSession.createTextMessage();
            dummyMessage.setJMSType("car");
            dummyMessage.setStringProperty("color", "red");
            dummyMessage.setLongProperty("weight", 3000);
            dummyMessage.setText("testSelectorExampleFromSpecs:1");
            sender.send(dummyMessage);
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setJMSType("car");
            message.setStringProperty("color", "blue");
            message.setLongProperty("weight", 3000);
            message.setText("testSelectorExampleFromSpecs:2");
            sender.send(message);
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertEquals("testSelectorExampleFromSpecs:2", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testGreaterThan() {
        try {
            receiverConnection.stop();
            receiver = receiverSession.createReceiver(receiverQueue, "weight > 2500");
            receiverConnection.start();
            javax.jms.TextMessage dummyMessage = senderSession.createTextMessage();
            dummyMessage.setLongProperty("weight", 1000);
            dummyMessage.setText("testGreaterThan:1");
            sender.send(dummyMessage);
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setLongProperty("weight", 3000);
            message.setText("testGreaterThan:2");
            sender.send(message);
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertEquals("testGreaterThan:2", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testEquals() {
        try {
            receiverConnection.stop();
            receiver = receiverSession.createReceiver(receiverQueue, "weight = 2500");
            receiverConnection.start();
            javax.jms.TextMessage dummyMessage = senderSession.createTextMessage();
            dummyMessage.setLongProperty("weight", 1000);
            dummyMessage.setText("testEquals:1");
            sender.send(dummyMessage);
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setLongProperty("weight", 2500);
            message.setText("testEquals:2");
            sender.send(message);
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertEquals("testEquals:2", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testNotEquals() {
        try {
            receiverConnection.stop();
            receiver = receiverSession.createReceiver(receiverQueue, "weight <> 2500");
            receiverConnection.start();
            javax.jms.TextMessage dummyMessage = senderSession.createTextMessage();
            dummyMessage.setLongProperty("weight", 2500);
            dummyMessage.setText("testEquals:1");
            sender.send(dummyMessage);
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setLongProperty("weight", 1000);
            message.setText("testEquals:2");
            sender.send(message);
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertEquals("testEquals:2", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testBetween() {
        try {
            receiverConnection.stop();
            receiver = receiverSession.createReceiver(receiverQueue, "age BETWEEN 15 and 19");
            receiverConnection.start();
            javax.jms.TextMessage dummyMessage = senderSession.createTextMessage();
            dummyMessage.setIntProperty("age", 20);
            dummyMessage.setText("testBetween:1");
            sender.send(dummyMessage);
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setIntProperty("age", 17);
            message.setText("testBetween:2");
            sender.send(message);
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertTrue("Message not received", (msg != null));
            junit.framework.TestCase.assertTrue(("Message of another test: " + (msg.getText())), msg.getText().startsWith("testBetween"));
            junit.framework.TestCase.assertEquals("testBetween:2", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testIn() {
        try {
            receiverConnection.stop();
            receiver = receiverSession.createReceiver(receiverQueue, "Country IN ('UK', 'US', 'France')");
            receiverConnection.start();
            javax.jms.TextMessage dummyMessage = senderSession.createTextMessage();
            dummyMessage.setStringProperty("Country", "Peru");
            dummyMessage.setText("testIn:1");
            sender.send(dummyMessage);
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setStringProperty("Country", "UK");
            message.setText("testIn:2");
            sender.send(message);
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertTrue("Message not received", (msg != null));
            junit.framework.TestCase.assertTrue(("Message of another test: " + (msg.getText())), msg.getText().startsWith("testIn"));
            junit.framework.TestCase.assertEquals("testIn:2", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testLikeEscape() {
        try {
            receiverConnection.stop();
            receiver = receiverSession.createReceiver(receiverQueue, "underscored LIKE \'\\_%\' ESCAPE \'\\\'");
            receiverConnection.start();
            javax.jms.TextMessage dummyMessage = senderSession.createTextMessage();
            dummyMessage.setStringProperty("underscored", "bar");
            dummyMessage.setText("testLikeEscape:1");
            sender.send(dummyMessage);
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setStringProperty("underscored", "_foo");
            message.setText("testLikeEscape:2");
            sender.send(message);
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertTrue("Message not received", (msg != null));
            junit.framework.TestCase.assertTrue(("Message of another test: " + (msg.getText())), msg.getText().startsWith("testLikeEscape"));
            junit.framework.TestCase.assertEquals("testLikeEscape:2", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testLike_2() {
        try {
            receiverConnection.stop();
            receiver = receiverSession.createReceiver(receiverQueue, "word LIKE 'l_se'");
            receiverConnection.start();
            javax.jms.TextMessage dummyMessage = senderSession.createTextMessage();
            dummyMessage.setStringProperty("word", "loose");
            dummyMessage.setText("testLike_2:1");
            sender.send(dummyMessage);
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setStringProperty("word", "lose");
            message.setText("testLike_2:2");
            sender.send(message);
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertTrue("Message not received", (msg != null));
            junit.framework.TestCase.assertTrue(("Message of another test: " + (msg.getText())), msg.getText().startsWith("testLike_2"));
            junit.framework.TestCase.assertEquals("testLike_2:2", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testLike_1() {
        try {
            receiverConnection.stop();
            receiver = receiverSession.createReceiver(receiverQueue, "phone LIKE '12%3'");
            receiverConnection.start();
            javax.jms.TextMessage dummyMessage = senderSession.createTextMessage();
            dummyMessage.setStringProperty("phone", "1234");
            dummyMessage.setText("testLike_1:1");
            sender.send(dummyMessage);
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setStringProperty("phone", "12993");
            message.setText("testLike_1:2");
            sender.send(message);
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertTrue("Message not received", (msg != null));
            junit.framework.TestCase.assertTrue(("Message of another test: " + (msg.getText())), msg.getText().startsWith("testLike_1"));
            junit.framework.TestCase.assertEquals("testLike_1:2", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testNull() {
        try {
            receiverConnection.stop();
            receiver = receiverSession.createReceiver(receiverQueue, "prop_name IS NULL");
            receiverConnection.start();
            javax.jms.TextMessage dummyMessage = senderSession.createTextMessage();
            dummyMessage.setStringProperty("prop_name", "not null");
            dummyMessage.setText("testNull:1");
            sender.send(dummyMessage);
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setText("testNull:2");
            sender.send(message);
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertTrue((msg != null));
            junit.framework.TestCase.assertEquals("testNull:2", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }
}

