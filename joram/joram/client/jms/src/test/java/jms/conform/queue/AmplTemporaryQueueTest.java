package jms.conform.queue;

public class AmplTemporaryQueueTest extends jms.framework.PTPTestCase {
    private javax.jms.TemporaryQueue tempQueue;

    private javax.jms.QueueReceiver tempReceiver;

    @org.junit.jupiter.api.Test
    public void testTemporaryQueue() {
        try {
            senderConnection.stop();
            receiverConnection.stop();
            tempQueue = receiverSession.createTemporaryQueue();
            sender = senderSession.createSender(null);
            tempReceiver = receiverSession.createReceiver(tempQueue);
            receiverConnection.start();
            senderConnection.start();
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setText("testTemporaryQueue");
            sender.send(tempQueue, message);
            javax.jms.Message m = tempReceiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue((m instanceof javax.jms.TextMessage));
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (m));
            junit.framework.TestCase.assertEquals("testTemporaryQueue", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }
}

