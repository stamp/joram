package jms.conform.connection;


public class AmplConnectionTest extends jms.framework.PTPTestCase {
    @org.junit.jupiter.api.Test
    public void testAcknowledge_failAssert0() throws java.lang.Exception {
        try {
            {
                receiverConnection.stop();
                receiverSession = receiverConnection.createQueueSession(false, javax.jms.Session.CLIENT_ACKNOWLEDGE);
                receiver = receiverSession.createReceiver(receiverQueue);
                receiverConnection.start();
                javax.jms.Message message = senderSession.createMessage();
                sender.send(message);
                javax.jms.Message m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
                receiverConnection.close();
                m.acknowledge();
                java.lang.String String_0 = "4.3.5 Invoking the acknowledge method of a received message from a closed " + "connection\'s session must throw a [javax.jms.]IllegalStateException.\n";
            }
            junit.framework.TestCase.fail("testAcknowledge should have thrown IllegalStateException");
        } catch (javax.jms.IllegalStateException expected) {
            junit.framework.TestCase.assertEquals("Forbidden call on a closed session.", expected.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    public void testUseClosedConnection_failAssert0() throws java.lang.Exception {
        try {
            {
                senderConnection.close();
                senderConnection.createQueueSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
            }
            junit.framework.TestCase.fail("testUseClosedConnection should have thrown IllegalStateException");
        } catch (javax.jms.IllegalStateException expected) {
            junit.framework.TestCase.assertEquals("Forbidden call on a closed connection.", expected.getMessage());
        }
    }

    public AmplConnectionTest() {
        super();
    }
}

