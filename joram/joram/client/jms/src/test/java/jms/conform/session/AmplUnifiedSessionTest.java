package jms.conform.session;

public class AmplUnifiedSessionTest extends jms.framework.UnifiedTestCase {
    protected javax.jms.QueueConnection queueConnection;

    protected javax.jms.QueueSession queueSession;

    protected javax.jms.TopicConnection topicConnection;

    protected javax.jms.TopicSession topicSession;

    @org.junit.jupiter.api.Test
    public void testCreateDurableConnectionConsumerOnQueueConnection() {
        try {
            queueConnection.createDurableConnectionConsumer(topic, "subscriptionName", "", ((javax.jms.ServerSessionPool) (null)), 1);
            junit.framework.TestCase.fail("Should throw a javax.jms.IllegalStateException");
        } catch (javax.jms.IllegalStateException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.IllegalStateException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testCreateDurableSubscriberOnQueueSession() {
        try {
            queueSession.createDurableSubscriber(topic, "subscriptionName");
            junit.framework.TestCase.fail("Should throw a javax.jms.IllegalStateException");
        } catch (javax.jms.IllegalStateException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.IllegalStateException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testCreateTemporaryTopicOnQueueSession() {
        try {
            javax.jms.TemporaryTopic tempTopic = queueSession.createTemporaryTopic();
            junit.framework.TestCase.fail("Should throw a javax.jms.IllegalStateException");
        } catch (javax.jms.IllegalStateException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.IllegalStateException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testCreateTopicOnQueueSession() {
        try {
            javax.jms.Topic tempTopic = queueSession.createTopic("topic_name");
            junit.framework.TestCase.fail("Should throw a javax.jms.IllegalStateException");
        } catch (javax.jms.IllegalStateException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.IllegalStateException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testUnsubscribeOnQueueSession() {
        try {
            queueSession.unsubscribe("subscriptionName");
            junit.framework.TestCase.fail("Should throw a javax.jms.IllegalStateException");
        } catch (javax.jms.IllegalStateException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.IllegalStateException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testCreateBrowserOnTopicSession() {
        try {
            javax.jms.QueueBrowser queueBrowser = topicSession.createBrowser(queue);
            junit.framework.TestCase.fail("Should throw a javax.jms.IllegalStateException");
        } catch (javax.jms.IllegalStateException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.IllegalStateException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testCreateQueueOnTopicSession() {
        try {
            javax.jms.Queue tempQueue = topicSession.createQueue("queue_name");
            junit.framework.TestCase.fail("Should throw a javax.jms.IllegalStateException");
        } catch (javax.jms.IllegalStateException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.IllegalStateException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testCreateTemporaryQueueOnTopicSession() {
        try {
            javax.jms.TemporaryQueue tempQueue = topicSession.createTemporaryQueue();
            junit.framework.TestCase.fail("Should throw a javax.jms.IllegalStateException");
        } catch (javax.jms.IllegalStateException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.IllegalStateException, not a " + e));
        }
    }

    public void setUp() {
        super.setUp();
        try {
            queueConnection = queueConnectionFactory.createQueueConnection();
            queueSession = queueConnection.createQueueSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
            topicConnection = topicConnectionFactory.createTopicConnection();
            topicSession = topicConnection.createTopicSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
            queueConnection.start();
            topicConnection.start();
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }

    public void tearDown() {
        try {
            queueConnection.close();
            topicConnection.close();
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        } finally {
            queueConnection = null;
            queueSession = null;
            topicConnection = null;
            topicSession = null;
            super.tearDown();
        }
    }
}
