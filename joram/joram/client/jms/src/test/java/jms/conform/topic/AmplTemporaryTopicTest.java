package jms.conform.topic;

public class AmplTemporaryTopicTest extends jms.framework.PubSubTestCase {
    private javax.jms.TemporaryTopic tempTopic;

    private javax.jms.TopicSubscriber tempSubscriber;

    @org.junit.jupiter.api.Test
    public void testTemporaryTopic() {
        try {
            tempTopic = subscriberSession.createTemporaryTopic();
            publisher = publisherSession.createPublisher(tempTopic);
            tempSubscriber = subscriberSession.createSubscriber(tempTopic);
            subscriberConnection.start();
            publisherConnection.start();
            javax.jms.TextMessage message = publisherSession.createTextMessage();
            message.setText("testTemporaryTopic");
            publisher.publish(message);
            javax.jms.Message m = tempSubscriber.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue((m instanceof javax.jms.TextMessage));
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (m));
            junit.framework.TestCase.assertEquals("testTemporaryTopic", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }
}
