package jms.conform.message;

public class AmplMessageBodyTest extends jms.framework.PTPTestCase {
    @org.junit.jupiter.api.Test
    public void testClearBody_2() {
        try {
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setStringProperty("prop", "foo");
            message.clearBody();
            junit.framework.TestCase.assertEquals("3.11.1 Clearing a message\'s body does not clear its property entries.\n", "foo", message.getStringProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testClearBody_1() {
        try {
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setText("bar");
            message.clearBody();
            junit.framework.TestCase.assertEquals(("3 .11.1 the clearBody method of Message resets the value of the message body " + ("to the 'empty' initial message value as set by the message type's create " + "method provided by Session.\n")), null, message.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testWriteOnReceivedBody() {
        try {
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setText("foo");
            sender.send(message);
            javax.jms.Message m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue("The message should be an instance of TextMessage.\n", (m instanceof javax.jms.TextMessage));
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (m));
            msg.setText("bar");
            junit.framework.TestCase.fail("should raise a MessageNotWriteableException (3.11.2)");
        } catch (javax.jms.MessageNotWriteableException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testGetTextMessageBody() {
        try {
            java.lang.String content = "abc";
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setText(content);
            java.lang.String result = message.getBody(java.lang.String.class);
            junit.framework.TestCase.assertTrue(" Text Message body is the same ", content.equals(result));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testGetObjectMessageBody() {
        try {
            java.lang.String content = "checking getBody()";
            java.lang.Exception excep = new java.lang.Exception(content);
            javax.jms.ObjectMessage message = senderSession.createObjectMessage();
            message.setObject(excep);
            java.io.Serializable tmp = message.getBody(java.io.Serializable.class);
            junit.framework.TestCase.assertTrue(" Instance retrieved is the same as expected ", (tmp instanceof java.lang.Exception));
            java.lang.Exception result = ((java.lang.Exception) (tmp));
            junit.framework.TestCase.assertTrue(" Object Message body  content is the same as expected ", result.getMessage().equals(content));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testGetMapMessageBody() {
        try {
            java.lang.String content = "checking getBody()";
            javax.jms.MapMessage message = senderSession.createMapMessage();
            message.setObject("except", new java.lang.Integer(12));
            java.util.Map tmp = message.getBody(java.util.Map.class);
            junit.framework.TestCase.assertTrue(" Instance retrieved is the same as expected ", ((tmp.get("except")) instanceof java.lang.Integer));
            java.lang.Integer result = ((java.lang.Integer) (tmp.get("except")));
            junit.framework.TestCase.assertTrue(" Map Message body  content is the same as expected ", ((result.intValue()) == 12));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testGetBytesMessageBody() {
        try {
            byte[] content = new byte[20];
            content[3] = 12;
            javax.jms.BytesMessage message = senderSession.createBytesMessage();
            message.writeBytes(content);
            message.reset();
            byte[] result = message.getBody(byte[].class);
            junit.framework.TestCase.assertTrue(" get body result is not null ", (result != null));
            junit.framework.TestCase.assertTrue(" Bytes Message body  content is the same as expected ", ((result[3]) == 12));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }
}
