/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2013 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): 
 */
package jms.jms2;

import java.util.HashMap;

import javax.jms.BytesMessage;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.MessageFormatException;
import javax.jms.ObjectMessage;
import javax.jms.StreamMessage;
import javax.jms.TextMessage;

import org.junit.jupiter.api.Test;
import org.objectweb.joram.client.jms.Queue;
import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;

import junit.framework.TestCase;

/**
 * Test Message.getBody method exceptions.
 */
public class Jms4Test extends TestCase {

	@Test
	public void testJms() throws Exception {

		ConnectionFactory cf = TcpConnectionFactory.create("localhost", 16010);
		AdminModule.connect(cf, "root", "root");   
		User.create("anonymous", "anonymous", 0);
		Queue queue = Queue.create("Jms4Test");
		queue.setFreeReading();
		queue.setFreeWriting();
		AdminModule.disconnect();

		JMSContext context = cf.createContext();
		JMSProducer producer = context.createProducer();
		JMSConsumer consumer = context.createConsumer(queue);
		context.start();

		producer.send(queue, context.createTextMessage("message"));
		TextMessage recv1 = (TextMessage) consumer.receive();
		assertTrue("Message not received", (recv1 != null));
		// Call TextMessage.getBody(Boolean.class) to extract TextMessage as Boolean.
		// Expect MessageFormatException.
		try {
			recv1.getBody(Boolean.class);
			assertTrue("Expected MessageFormatException to be thrown", false);
		} catch (MessageFormatException e) {
		} catch (Exception e) {
			assertTrue("Caught unexpected exception: " + e.getMessage(), false);
		}

		producer.send(queue, context.createObjectMessage(new StringBuffer("message")));
		ObjectMessage recv2 = (ObjectMessage) consumer.receive();
		assertTrue("Message not received", (recv2 != null));
		// Call ObjectMessage.getBody(HashMap.class) to extract ObjectMessage as HashMap.
		// Expect MessageFormatException.
		try {
			recv2.getBody(HashMap.class);
			assertTrue("Expected MessageFormatException to be thrown", false);
		} catch (MessageFormatException e) {
		} catch (Exception e) {
			assertTrue("Caught unexpected exception: " + e.getMessage(), false);
		}

		StreamMessage msg1 = context.createStreamMessage();
		msg1.writeBoolean(true);
		msg1.writeLong((long) 123456789);
		producer.send(queue, msg1);
		StreamMessage recv3 = (StreamMessage) consumer.receive();
		assertTrue("Message not received", (recv3 != null));
		// Call StreamMessage.getBody(HashMap.class) to extract StreamMessage as HashMap.
		// Expect MessageFormatException.
		try {
			recv3.getBody(HashMap.class);
			assertTrue("Expected MessageFormatException to be thrown", false);
		} catch (MessageFormatException e) {
		} catch (Exception e) {
			assertTrue("Caught unexpected exception: " + e.getMessage(), false);
		}

		BytesMessage msg2 = context.createBytesMessage();
		msg2.writeBoolean(true);
		msg2.writeLong((long) 123456789);
		try {
			msg2.getBody(StringBuffer.class);
			assertTrue("Expected MessageFormatException to be thrown", false);
		} catch (MessageFormatException e) {
		} catch (Exception e) {
			assertTrue("Caught unexpected exception: " + e.getMessage(), false);
		}

		producer.send(queue, msg2);
		BytesMessage recv4 = (BytesMessage) consumer.receive();
		assertTrue("Message not received", (recv4 != null));
		// Call BytesMessage.getBody(StringBuffer.class) to receive BytesMessage as StringBuffer
		// Expect MessageFormatException.
		try {
			recv4.getBody(StringBuffer.class);
			assertTrue("Expected MessageFormatException to be thrown", false);
		} catch (MessageFormatException e) {
		} catch (Exception e) {
			assertTrue("Caught unexpected exception: " + e.getMessage(), false);
		}

		context.close();
	}

}
