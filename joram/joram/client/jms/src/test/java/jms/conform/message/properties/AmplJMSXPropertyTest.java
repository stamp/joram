package jms.conform.message.properties;

public class AmplJMSXPropertyTest extends jms.framework.PTPTestCase {
    @org.junit.jupiter.api.Test
    public void testSupportsJMSXGroupID() {
        try {
            boolean found = false;
            javax.jms.ConnectionMetaData metaData = senderConnection.getMetaData();
            java.util.Enumeration e = metaData.getJMSXPropertyNames();
            while (e.hasMoreElements()) {
                java.lang.String jmsxPropertyName = ((java.lang.String) (e.nextElement()));
                if (jmsxPropertyName.equals("JMSXGroupID")) {
                    found = true;
                }
            } 
            junit.framework.TestCase.assertTrue("JMSXGroupID property is not supported", found);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testJMSXGroupID_1() {
        try {
            java.lang.String groupID = "testSupportsJMSXGroupID_1:group";
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setStringProperty("JMSXGroupID", groupID);
            message.setText("testSupportsJMSXGroupID_1");
            sender.send(message);
            javax.jms.Message m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue((m instanceof javax.jms.TextMessage));
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (m));
            junit.framework.TestCase.assertEquals(groupID, msg.getStringProperty("JMSXGroupID"));
            junit.framework.TestCase.assertEquals("testSupportsJMSXGroupID_1", msg.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testSupportsJMSXGroupSeq() {
        try {
            boolean found = false;
            javax.jms.ConnectionMetaData metaData = senderConnection.getMetaData();
            java.util.Enumeration e = metaData.getJMSXPropertyNames();
            while (e.hasMoreElements()) {
                java.lang.String jmsxPropertyName = ((java.lang.String) (e.nextElement()));
                if (jmsxPropertyName.equals("JMSXGroupSeq")) {
                    found = true;
                }
            } 
            junit.framework.TestCase.assertTrue("JMSXGroupSeq property is not supported", found);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testSupportsJMSXDeliveryCount() {
        try {
            boolean found = false;
            javax.jms.ConnectionMetaData metaData = senderConnection.getMetaData();
            java.util.Enumeration e = metaData.getJMSXPropertyNames();
            while (e.hasMoreElements()) {
                java.lang.String jmsxPropertyName = ((java.lang.String) (e.nextElement()));
                if (jmsxPropertyName.equals("JMSXDeliveryCount")) {
                    found = true;
                }
            } 
            junit.framework.TestCase.assertTrue("JMSXDeliveryCount property is not supported", found);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testJMSXDeliveryCount() {
        try {
            senderConnection.stop();
            senderSession = senderConnection.createQueueSession(true, 0);
            junit.framework.TestCase.assertEquals(true, senderSession.getTransacted());
            sender = senderSession.createSender(senderQueue);
            senderConnection.start();
            receiverConnection.stop();
            receiverSession = receiverConnection.createQueueSession(true, 0);
            junit.framework.TestCase.assertEquals(true, receiverSession.getTransacted());
            receiver = receiverSession.createReceiver(receiverQueue);
            receiverConnection.start();
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setText("testJMSXDeliveryCount");
            sender.send(message);
            senderSession.commit();
            javax.jms.Message m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue((m != null));
            junit.framework.TestCase.assertTrue((m instanceof javax.jms.TextMessage));
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (m));
            junit.framework.TestCase.assertEquals("testJMSXDeliveryCount", msg.getText());
            junit.framework.TestCase.assertEquals(false, msg.getJMSRedelivered());
            int jmsxDeliveryCount = msg.getIntProperty("JMSXDeliveryCount");
            junit.framework.TestCase.assertEquals(1, jmsxDeliveryCount);
            receiverSession.rollback();
            m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue((m != null));
            junit.framework.TestCase.assertTrue((m instanceof javax.jms.TextMessage));
            msg = ((javax.jms.TextMessage) (m));
            junit.framework.TestCase.assertEquals("testJMSXDeliveryCount", msg.getText());
            junit.framework.TestCase.assertEquals(true, msg.getJMSRedelivered());
            jmsxDeliveryCount = msg.getIntProperty("JMSXDeliveryCount");
            junit.framework.TestCase.assertEquals(2, jmsxDeliveryCount);
        } catch (javax.jms.JMSException e) {
            fail(e);
        } catch (java.lang.Exception e) {
            fail(e);
        }
    }
}
