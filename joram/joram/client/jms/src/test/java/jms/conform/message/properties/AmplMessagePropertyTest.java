package jms.conform.message.properties;

public class AmplMessagePropertyTest extends jms.framework.PTPTestCase {
    @org.junit.jupiter.api.Test
    public void testSetObjectProperty_2() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setObjectProperty("prop", new java.util.Vector());
            junit.framework.TestCase.fail(("3.5.5 An attempt to use any other class [than Boolean, Byte,...,String] must throw " + "a JMS MessageFormatException.\n"));
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should throw a javax.jms.MessageFormatException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testSetObjectProperty_1() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setObjectProperty("pi", new java.lang.Float(3.14159F));
            junit.framework.TestCase.assertEquals(3.14159F, message.getFloatProperty("pi"), 0);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testGetObjectProperty() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            junit.framework.TestCase.assertEquals(("3.5.5 A null value is returned [by the getObjectProperty method] if a property by the specified " + "name does not exits.\n"), null, message.getObjectProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testGetStringProperty() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            junit.framework.TestCase.assertEquals(("3.5.5 A null value is returned [by the getStringProperty method] if a property by the specified " + "name does not exits.\n"), null, message.getStringProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testGetDoubleProperty() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.getDoubleProperty("prop");
            junit.framework.TestCase.fail("Should raise a NullPointerException.\n");
        } catch (java.lang.NullPointerException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testGetFloatProperty() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.getFloatProperty("prop");
            junit.framework.TestCase.fail("Should raise a NullPointerException.\n");
        } catch (java.lang.NullPointerException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testGetLongProperty() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.getLongProperty("prop");
            junit.framework.TestCase.fail("Should raise a NumberFormatException.\n");
        } catch (java.lang.NumberFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testGetIntProperty() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.getIntProperty("prop");
            junit.framework.TestCase.fail("Should raise a NumberFormatException.\n");
        } catch (java.lang.NumberFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testGetShortProperty() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.getShortProperty("prop");
            junit.framework.TestCase.fail("Should raise a NumberFormatException.\n");
        } catch (java.lang.NumberFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testGetByteProperty() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.getByteProperty("prop");
            junit.framework.TestCase.fail("Should raise a NumberFormatException.\n");
        } catch (java.lang.NumberFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testGetBooleanProperty() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            junit.framework.TestCase.assertEquals(false, message.getBooleanProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testGetPropertyNames() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setJMSCorrelationID("foo");
            java.util.Enumeration e = message.getPropertyNames();
            junit.framework.TestCase.assertTrue(("3.5.6 The getPropertyNames method does not return the names of " + "the JMS standard header field [e.g. JMSCorrelationID].\n"), (!(e.hasMoreElements())));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testPropertyIteration() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            java.util.Enumeration e = message.getPropertyNames();
            junit.framework.TestCase.assertTrue("No property yet defined.\n", (!(e.hasMoreElements())));
            message.setDoubleProperty("pi", 3.14159);
            e = message.getPropertyNames();
            junit.framework.TestCase.assertEquals("One property defined of name \'pi\'.\n", "pi", ((java.lang.String) (e.nextElement())));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testClearProperties_2() {
        try {
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setText("foo");
            message.clearProperties();
            junit.framework.TestCase.assertEquals("3.5.7 Clearing a message\'s  property entries does not clear the value of its body.\n", "foo", message.getText());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testClearProperties_1() {
        try {
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setStringProperty("prop", "foo");
            message.clearProperties();
            junit.framework.TestCase.assertEquals("3.5.7 A message\'s properties are deleted by the clearProperties method.\n", null, message.getStringProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }
}
