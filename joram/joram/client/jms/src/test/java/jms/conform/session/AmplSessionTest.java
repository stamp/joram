package jms.conform.session;

public class AmplSessionTest extends jms.framework.PTPTestCase {
    @org.junit.jupiter.api.Test
    public void testRecoverTransactedSession() {
        try {
            junit.framework.TestCase.assertEquals(false, senderSession.getTransacted());
            senderSession = senderConnection.createQueueSession(true, 0);
            junit.framework.TestCase.assertEquals(true, senderSession.getTransacted());
            senderSession.recover();
            junit.framework.TestCase.fail("Should raise an IllegalStateException, the session is not transacted.\n");
        } catch (javax.jms.IllegalStateException e) {
        } catch (java.lang.IllegalStateException e) {
            junit.framework.TestCase.fail("Should raise a javax.jms.IllegalStateException, not a java.lang.IllegalStateException.\n");
        } catch (java.lang.Exception e) {
            junit.framework.TestCase.fail(("Should raise a javax.jms.IllegalStateException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testRollbackTransactedSession() {
        try {
            senderSession = senderConnection.createQueueSession(true, 0);
            sender = senderSession.createSender(senderQueue);
            junit.framework.TestCase.assertEquals(true, senderSession.getTransacted());
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setText("testRollbackTransactedSession");
            sender.send(message);
            senderSession.rollback();
            javax.jms.TextMessage m = ((javax.jms.TextMessage) (receiver.receiveNoWait()));
            junit.framework.TestCase.assertEquals(null, m);
        } catch (java.lang.Exception e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testCommitTransactedSession() {
        try {
            senderSession = senderConnection.createQueueSession(true, 0);
            sender = senderSession.createSender(senderQueue);
            junit.framework.TestCase.assertEquals(true, senderSession.getTransacted());
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setText("testCommitTransactedSession");
            sender.send(message);
            javax.jms.TextMessage m = ((javax.jms.TextMessage) (receiver.receiveNoWait()));
            junit.framework.TestCase.assertEquals(null, m);
            senderSession.commit();
            m = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            junit.framework.TestCase.assertTrue((m != null));
            junit.framework.TestCase.assertEquals("testCommitTransactedSession", m.getText());
        } catch (java.lang.Exception e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testRollbackNonTransactedSession() {
        try {
            junit.framework.TestCase.assertEquals(false, senderSession.getTransacted());
            senderSession.rollback();
            junit.framework.TestCase.fail("Should raise an IllegalStateException, the session is not transacted.\n");
        } catch (javax.jms.IllegalStateException e) {
        } catch (java.lang.IllegalStateException e) {
            junit.framework.TestCase.fail("Should raise a javax.jms.IllegalStateException, not a java.lang.IllegalStateException.\n");
        } catch (java.lang.Exception e) {
            junit.framework.TestCase.fail(("Should raise a javax.jms.IllegalStateException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testCommitNonTransactedSession() {
        try {
            junit.framework.TestCase.assertEquals(false, senderSession.getTransacted());
            senderSession.commit();
            junit.framework.TestCase.fail("Should raise an IllegalStateException, the session is not transacted.\n");
        } catch (javax.jms.IllegalStateException e) {
        } catch (java.lang.IllegalStateException e) {
            junit.framework.TestCase.fail("Should raise a javax.jms.IllegalStateException, not a java.lang.IllegalStateException.\n");
        } catch (java.lang.Exception e) {
            junit.framework.TestCase.fail(("Should raise a javax.jms.IllegalStateException, not a " + e));
        }
    }

    @org.junit.jupiter.api.Test
    public void testGetTransacted() {
        try {
            junit.framework.TestCase.assertEquals(false, senderSession.getTransacted());
            senderSession = senderConnection.createQueueSession(true, javax.jms.Session.AUTO_ACKNOWLEDGE);
            junit.framework.TestCase.assertEquals(true, senderSession.getTransacted());
        } catch (java.lang.Exception e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testAcknowledge() {
        try {
            receiverSession = receiverConnection.createQueueSession(false, javax.jms.Session.CLIENT_ACKNOWLEDGE);
            receiver = receiverSession.createReceiver(receiverQueue);
            javax.jms.Message message = senderSession.createMessage();
            sender.send(message);
            javax.jms.Message m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            receiverSession.close();
            m.acknowledge();
            junit.framework.TestCase.fail(("4.4.1 Invoking the acknowledge method of a received message from a closed " + " session must throw an [javax.jms.]IllegalStateException.\n"));
        } catch (javax.jms.IllegalStateException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should raise a javax.jms.IllegalStateException, not a " + e));
        } catch (java.lang.IllegalStateException e) {
            junit.framework.TestCase.fail(("4.4.1 Invoking the acknowledge method of a received message from a closed " + ("session must throw an [javax.jms.]IllegalStateException, " + "[not a java.lang.IllegalStateException]")));
        }
    }

    @org.junit.jupiter.api.Test
    public void testUseMessage() {
        try {
            javax.jms.TextMessage message = senderSession.createTextMessage();
            message.setText("testUseMessage");
            sender.send(message);
            javax.jms.TextMessage m = ((javax.jms.TextMessage) (receiver.receive(jms.framework.TestConfig.TIMEOUT)));
            receiverSession.close();
            junit.framework.TestCase.assertEquals("testUseMessage", m.getText());
        } catch (java.lang.Exception e) {
            junit.framework.TestCase.fail(("4.4.1 It is valid to continue to use message objects created or received via " + "the [closed] session.\n"));
        }
    }

    @org.junit.jupiter.api.Test
    public void testUsedClosedSession() {
        try {
            senderSession.close();
            senderSession.createMessage();
            junit.framework.TestCase.fail("4.4.1 An attempt to use [a closed session] must throw a [javax.jms.]IllegalStateException.\n");
        } catch (javax.jms.IllegalStateException e) {
        } catch (javax.jms.JMSException e) {
            junit.framework.TestCase.fail(("Should raise a javax.jms.IllegalStateException, not a " + e));
        } catch (java.lang.IllegalStateException e) {
            junit.framework.TestCase.fail("Should raise a javax.jms.IllegalStateException, not a java.lang.IllegalStateException");
        }
    }

    @org.junit.jupiter.api.Test
    public void testCloseClosedSession() {
        try {
            senderSession.close();
            senderSession.close();
        } catch (java.lang.Exception e) {
            junit.framework.TestCase.fail("4.4.1 Closing a closed session must NOT throw an exception.\n");
        }
    }
}

