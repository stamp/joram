package jms.conform.message.headers;


public class AmplMessageHeaderTest extends jms.framework.PTPTestCase {
    @org.junit.jupiter.api.Test
    public void testJMSPriority_2() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            sender.send(message);
            sender.setPriority(9);
            sender.send(message);
            junit.framework.TestCase.assertEquals(("3.4.9 After completion of the send it holds the value specified by the " + "method sending the message.\n"), 9, message.getJMSPriority());
            javax.jms.Message msg = receiver.receive(jms.framework.TestConfig.TIMEOUT);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testJMSPriority_1() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setJMSPriority(0);
            sender.send(message);
            junit.framework.TestCase.assertTrue("3.4.9 When a message is sent this value is ignored.\n", ((message.getJMSPriority()) != 0));
            junit.framework.TestCase.assertEquals(("3.4.9 After completion of the send it holds the value specified by the " + "method sending the message.\n"), javax.jms.Message.DEFAULT_PRIORITY, message.getJMSPriority());
            javax.jms.Message msg = receiver.receive(jms.framework.TestConfig.TIMEOUT);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testJMSExpiration() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            sender.send(message);
            javax.jms.Message msg = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertEquals(("3.4.9 When a message is received its JMSExpiration header field contains this same " + "value [i.e. set on return of the send method].\n"), message.getJMSExpiration(), msg.getJMSExpiration());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testJMSMessageID_2() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            sender.send(message);
            junit.framework.TestCase.assertTrue("3.4.3 When the send method returns it contains a provider-assigned value.\n", ((message.getJMSMessageID()) != null));
            junit.framework.TestCase.assertTrue("3.4.3 All JMSMessageID values must start with the prefix \'ID:\'.\n", message.getJMSMessageID().startsWith("ID:"));
            javax.jms.Message msg = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue("3.4.3 All JMSMessageID values must start with the prefix \'ID:\'.\n", msg.getJMSMessageID().startsWith("ID:"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testJMSMessageID_1() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setJMSMessageID("ID:foo");
            sender.send(message);
            junit.framework.TestCase.assertTrue("3.4.3 When a message is sent this value is ignored.\n", ((message.getJMSMessageID()) != "ID:foo"));
            javax.jms.Message msg = receiver.receive(jms.framework.TestConfig.TIMEOUT);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testJMSDeliveryMode() {
        try {
            junit.framework.TestCase.assertEquals(javax.jms.DeliveryMode.PERSISTENT, sender.getDeliveryMode());
            javax.jms.Message message = senderSession.createMessage();
            message.setJMSDeliveryMode(javax.jms.DeliveryMode.NON_PERSISTENT);
            sender.send(message);
            junit.framework.TestCase.assertTrue("3.4.2 When a message is sent this value is ignored", ((message.getJMSDeliveryMode()) != (javax.jms.DeliveryMode.NON_PERSISTENT)));
            junit.framework.TestCase.assertEquals(("3.4.2 After completion of the send it holds the delivery mode specified " + "by the sending method (persistent by default).\n"), javax.jms.Message.DEFAULT_DELIVERY_MODE, message.getJMSDeliveryMode());
            javax.jms.Message msg = receiver.receive(jms.framework.TestConfig.TIMEOUT);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testJMSDestination() {
        try {
            jms.admin.Admin admin = getJoramAdmin();
            admin.createQueue("anotherQueue");
            javax.jms.Queue anotherQueue = admin.createQueue("anotherQueue");
            junit.framework.TestCase.assertTrue((anotherQueue != (senderQueue)));
            javax.jms.Message message = senderSession.createMessage();
            message.setJMSDestination(anotherQueue);
            sender.send(message);
            junit.framework.TestCase.assertTrue("3.4.1 When a message is sent this value is ignored.\n", ((message.getJMSDestination()) != anotherQueue));
            junit.framework.TestCase.assertEquals(("3.4.1 After completion of the send it holds the destination object specified " + "by the sending method.\n"), senderQueue, message.getJMSDestination());
            javax.jms.Message msg = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertEquals(("3.4.1 When a message is received, its destination value must be equivalent  " + " to the value assigned when it was sent.\n"), ((javax.jms.Queue) (message.getJMSDestination())).getQueueName(), ((javax.jms.Queue) (msg.getJMSDestination())).getQueueName());
            admin.deleteQueue(anotherQueue);
        } catch (java.lang.Exception e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testJMSReplyTo_1() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setJMSReplyTo(senderQueue);
            sender.send(message);
            javax.jms.Message msg = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            javax.jms.Destination dest = msg.getJMSDestination();
            junit.framework.TestCase.assertTrue("JMS ReplyTo header field should be a Queue", (dest instanceof javax.jms.Queue));
            javax.jms.Queue replyTo = ((javax.jms.Queue) (dest));
            junit.framework.TestCase.assertEquals("JMS ReplyTo header field should be equals to the sender queue", ((javax.jms.Queue) (replyTo)).getQueueName(), ((javax.jms.Queue) (senderQueue)).getQueueName());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testJMSReplyTo_2() {
        try {
            javax.jms.TemporaryQueue tempQueue = senderSession.createTemporaryQueue();
            javax.jms.Message message = senderSession.createMessage();
            message.setJMSReplyTo(tempQueue);
            sender.send(message);
            javax.jms.Message msg = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            javax.jms.Destination dest = msg.getJMSReplyTo();
            junit.framework.TestCase.assertTrue("JMS ReplyTo header field should be a TemporaryQueue", (dest instanceof javax.jms.TemporaryQueue));
            javax.jms.Queue replyTo = ((javax.jms.Queue) (dest));
            junit.framework.TestCase.assertEquals("JMS ReplyTo header field should be equals to the temporary queue", ((javax.jms.Queue) (replyTo)).getQueueName(), ((javax.jms.Queue) (tempQueue)).getQueueName());
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }
}

