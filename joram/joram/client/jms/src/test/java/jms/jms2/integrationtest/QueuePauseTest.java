/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C)  2018 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s):
 * Contributor(s): 
 */
package jms.jms2.integrationtest;

import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.objectweb.joram.client.jms.Queue;
import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;
import org.ow2.joram.testutils.AgentManager;

import junit.framework.TestCase;

/**
 * Test the queue pause mechanism.
 */
public class QueuePauseTest extends TestCase implements javax.jms.MessageListener {
  
  JMSContext context;
  JMSConsumer consumer;
  
  ConnectionFactory cf = null;
  Queue dest = null;
  
  public void setUp() throws Exception {
    cf = TcpConnectionFactory.create("localhost", 16010);
    AdminModule.connect(cf, "root", "root");   
    User.create("anonymous", "anonymous", 0);
    dest = Queue.create("QueuePauseTest");
    dest.setFreeReading();
    dest.setFreeWriting();
    AdminModule.disconnect();
  }

  public void testQueuePause() throws Exception {

	  pause(true);
	  deliverMessage();
	  pause(true);

	  try {
		  AgentManager.restartAgentServer();
	  } catch(Exception e) {
		  fail(e.getMessage());
	  }

	  System.out.println("Server restarted.");
	  deliverMessage();

	  assertTrue("Should receive 2 messages: " + nbrecv, (nbrecv == 2));
  }

  void pause(boolean pause) throws Exception {
    cf = TcpConnectionFactory.create("localhost", 16010);
    AdminModule.connect(cf, "root", "root");   
    dest = Queue.create("QueuePauseTest");
    dest.setPause(pause);
    AdminModule.disconnect();
  }
  
  boolean received;

  void deliverMessage() throws Exception {
    context = cf.createContext(JMSContext.AUTO_ACKNOWLEDGE);
    consumer = context.createConsumer(dest);
    consumer.setMessageListener(this);
    context.start();

    JMSContext prodCtx = cf.createContext();
    JMSProducer producer = prodCtx.createProducer();
    producer.setDeliveryMode(DeliveryMode.PERSISTENT);
    TextMessage msg = prodCtx.createTextMessage("test pause");
    received = false;
    producer.send(dest, msg);

    // Wait to receive the message.
    Thread.sleep(10000);

    assertFalse("The message is received before starting queue ", received);

    pause(false);
    
    // Wait to receive the message.
    Thread.sleep(5000);
    assertTrue("The message is not received after starting queue ", received);
    
    context.close();
    prodCtx.close();
  }
  
  int nbrecv = 0;

  public void onMessage(Message message) {
    try {
      nbrecv += 1;
      System.out.println("#" + nbrecv + " - " + message.getJMSMessageID() + ", JMSRedelivered = " + message.getJMSRedelivered() + 
                         ", JMSTimestamp = " + message.getJMSTimestamp());
      System.out.println(System.currentTimeMillis() + ": message received deliveryTime = " + message.getJMSDeliveryTime());
    } catch (JMSException e) {
      e.printStackTrace();
    }
    received = true;
  }

}
