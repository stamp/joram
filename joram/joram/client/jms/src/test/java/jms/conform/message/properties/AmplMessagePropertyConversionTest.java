package jms.conform.message.properties;

public class AmplMessagePropertyConversionTest extends jms.framework.PTPTestCase {
    @org.junit.jupiter.api.Test
    public void testString2String() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setStringProperty("pi", "3.14159");
            junit.framework.TestCase.assertEquals("3.14159", message.getStringProperty("pi"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testString2Double_2() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setStringProperty("pi", "not_a_number");
            message.getDoubleProperty("pi");
            junit.framework.TestCase.fail(("3.5.4 The String to numeric conversions must throw the java.lang.NumberFormatException " + " if the numeric\'s valueOf() method does not accept the String value as a valid representation.\n"));
        } catch (java.lang.NumberFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testString2Double_1() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setStringProperty("pi", "3.14159");
            junit.framework.TestCase.assertEquals(3.14159, message.getDoubleProperty("pi"), 0);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testString2Float_2() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setStringProperty("pi", "not_a_number");
            message.getFloatProperty("pi");
            junit.framework.TestCase.fail(("3.5.4 The String to numeric conversions must throw the java.lang.NumberFormatException " + " if the numeric\'s valueOf() method does not accept the String value as a valid representation.\n"));
        } catch (java.lang.NumberFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testString2Float_1() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setStringProperty("pi", "3.14159");
            junit.framework.TestCase.assertEquals(3.14159F, message.getFloatProperty("pi"), 0);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testString2Long_2() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setStringProperty("pi", "3.14159");
            message.getLongProperty("pi");
            junit.framework.TestCase.fail(("3.5.4 The String to numeric conversions must throw the java.lang.NumberFormatException " + " if the numeric\'s valueOf() method does not accept the String value as a valid representation.\n"));
        } catch (java.lang.NumberFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testString2Long_1() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setStringProperty("prop", "0");
            junit.framework.TestCase.assertEquals(((long) (0)), message.getLongProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testString2Int_2() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setStringProperty("pi", "3.14159");
            message.getIntProperty("pi");
            junit.framework.TestCase.fail(("3.5.4 The String to numeric conversions must throw the java.lang.NumberFormatException " + " if the numeric\'s valueOf() method does not accept the String value as a valid representation.\n"));
        } catch (java.lang.NumberFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testString2Int_1() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setStringProperty("prop", "0");
            junit.framework.TestCase.assertEquals(((int) (0)), message.getIntProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testString2Short_2() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setStringProperty("pi", "3.14159");
            message.getShortProperty("pi");
            junit.framework.TestCase.fail(("3.5.4 The String to numeric conversions must throw the java.lang.NumberFormatException " + " if the numeric\'s valueOf() method does not accept the String value as a valid representation.\n"));
        } catch (java.lang.NumberFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testString2Short_1() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setStringProperty("prop", "0");
            junit.framework.TestCase.assertEquals(((short) (0)), message.getShortProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testString2Byte_2() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setStringProperty("pi", "3.14159");
            message.getByteProperty("pi");
            junit.framework.TestCase.fail(("3.5.4 The String to numeric conversions must throw the java.lang.NumberFormatException " + " if the numeric\'s valueOf() method does not accept the String value as a valid representation.\n"));
        } catch (java.lang.NumberFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testString2Byte_1() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setStringProperty("prop", "0");
            junit.framework.TestCase.assertEquals(((byte) (0)), message.getByteProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testString2Boolean_2() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setStringProperty("prop", "test");
            junit.framework.TestCase.assertEquals(false, message.getBooleanProperty("prop"));
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testString2Boolean_1() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setStringProperty("prop", "true");
            junit.framework.TestCase.assertEquals(true, message.getBooleanProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testDouble2String() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setDoubleProperty("prop", 127.0);
            junit.framework.TestCase.assertEquals("127.0", message.getStringProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testDouble2Double() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setDoubleProperty("prop", 127.0);
            junit.framework.TestCase.assertEquals(127.0, message.getDoubleProperty("prop"), 0);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testDouble2Float() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setDoubleProperty("prop", 127.0);
            message.getFloatProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testDouble2Long() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setDoubleProperty("prop", 127.0);
            message.getLongProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testDouble2Int() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setDoubleProperty("prop", 127.0);
            message.getIntProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testDouble2Short() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setDoubleProperty("prop", 127.0);
            message.getShortProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testDouble2Byte() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setDoubleProperty("prop", 127.0);
            message.getByteProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testDouble2Boolean() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setDoubleProperty("prop", 127.0);
            message.getBooleanProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testFloat2String() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setFloatProperty("prop", 127.0F);
            junit.framework.TestCase.assertEquals("127.0", message.getStringProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testFloat2Double() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setFloatProperty("prop", 127.0F);
            junit.framework.TestCase.assertEquals(127.0, message.getDoubleProperty("prop"), 0);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testFloat2Float() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setFloatProperty("prop", 127.0F);
            junit.framework.TestCase.assertEquals(127.0F, message.getFloatProperty("prop"), 0);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testFloat2Long() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setFloatProperty("prop", 127.0F);
            message.getLongProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testFloat2Int() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setFloatProperty("prop", 127.0F);
            message.getIntProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testFloat2Short() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setFloatProperty("prop", 127.0F);
            message.getShortProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testFloat2Byte() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setFloatProperty("prop", 127.0F);
            message.getByteProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testFloat2Boolean() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setFloatProperty("prop", 127.0F);
            message.getBooleanProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testLong2String() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setLongProperty("prop", 127L);
            junit.framework.TestCase.assertEquals("127", message.getStringProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testLong2Double() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setLongProperty("prop", 127L);
            message.getDoubleProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testLong2Float() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setLongProperty("prop", 127L);
            message.getFloatProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testLong2Long() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setLongProperty("prop", 127L);
            junit.framework.TestCase.assertEquals(127L, message.getLongProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testLong2Int() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setLongProperty("prop", 127L);
            message.getIntProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testLong2Short() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setLongProperty("prop", 127L);
            message.getShortProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testLong2Byte() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setLongProperty("prop", 127L);
            message.getByteProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testLong2Boolean() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setLongProperty("prop", 127L);
            message.getBooleanProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testInt2String() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setIntProperty("prop", ((int) (127)));
            junit.framework.TestCase.assertEquals("127", message.getStringProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testInt2Double() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setIntProperty("prop", ((int) (127)));
            message.getDoubleProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testInt2Float() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setIntProperty("prop", ((int) (127)));
            message.getFloatProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testInt2Long() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setIntProperty("prop", ((int) (127)));
            junit.framework.TestCase.assertEquals(127L, message.getLongProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testInt2Int() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setIntProperty("prop", ((int) (127)));
            junit.framework.TestCase.assertEquals(((int) (127)), message.getIntProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testInt2Short() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setIntProperty("prop", java.lang.Integer.MAX_VALUE);
            message.getShortProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testInt2Byte() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setIntProperty("prop", java.lang.Integer.MAX_VALUE);
            message.getByteProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testInt2Boolean() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setIntProperty("prop", java.lang.Integer.MAX_VALUE);
            message.getBooleanProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testShort2String() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setShortProperty("prop", ((short) (127)));
            junit.framework.TestCase.assertEquals("127", message.getStringProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testShort2Double() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setShortProperty("prop", ((short) (127)));
            message.getDoubleProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testShort2Float() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setShortProperty("prop", ((short) (127)));
            message.getFloatProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testShort2Long() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setShortProperty("prop", ((short) (127)));
            junit.framework.TestCase.assertEquals(127L, message.getLongProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testShort2Int() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setShortProperty("prop", ((short) (127)));
            junit.framework.TestCase.assertEquals(((int) (127)), message.getIntProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testShort2Short() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setShortProperty("prop", ((short) (127)));
            junit.framework.TestCase.assertEquals(((short) (127)), message.getShortProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testShort2Byte() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setShortProperty("prop", ((short) (127)));
            message.getByteProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testShort2Boolean() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setShortProperty("prop", ((short) (127)));
            message.getBooleanProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testByte2String() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setByteProperty("prop", ((byte) (127)));
            junit.framework.TestCase.assertEquals("127", message.getStringProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testByte2Double() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setByteProperty("prop", ((byte) (127)));
            message.getDoubleProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testByte2Float() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setByteProperty("prop", ((byte) (127)));
            message.getFloatProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testByte2Long() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setByteProperty("prop", ((byte) (127)));
            junit.framework.TestCase.assertEquals(127L, message.getLongProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testByte2Int() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setByteProperty("prop", ((byte) (127)));
            junit.framework.TestCase.assertEquals(((int) (127)), message.getIntProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testByte2Short() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setByteProperty("prop", ((byte) (127)));
            junit.framework.TestCase.assertEquals(((short) (127)), message.getShortProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testByte2Byte() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setByteProperty("prop", ((byte) (127)));
            junit.framework.TestCase.assertEquals(((byte) (127)), message.getByteProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testByte2Boolean() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setByteProperty("prop", ((byte) (127)));
            message.getBooleanProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testBoolean2String() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setBooleanProperty("prop", true);
            junit.framework.TestCase.assertEquals("true", message.getStringProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testBoolean2Double() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setBooleanProperty("prop", true);
            message.getDoubleProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testBoolean2Float() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setBooleanProperty("prop", true);
            message.getFloatProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testBoolean2Long() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setBooleanProperty("prop", true);
            message.getLongProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testBoolean2Int() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setBooleanProperty("prop", true);
            message.getIntProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testBoolean2Short() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setBooleanProperty("prop", true);
            message.getShortProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testBoolean2Byte() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setBooleanProperty("prop", true);
            message.getByteProperty("prop");
            junit.framework.TestCase.fail("3.5.4 The unmarked cases [of Table 0-4] should raise a JMS MessageFormatException.\n");
        } catch (javax.jms.MessageFormatException e) {
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testBoolean2Boolean() {
        try {
            javax.jms.Message message = senderSession.createMessage();
            message.setBooleanProperty("prop", true);
            junit.framework.TestCase.assertEquals(true, message.getBooleanProperty("prop"));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }
}

