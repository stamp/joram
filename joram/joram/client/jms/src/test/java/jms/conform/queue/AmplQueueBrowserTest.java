package jms.conform.queue;

public class AmplQueueBrowserTest extends jms.framework.PTPTestCase {
    protected javax.jms.QueueBrowser receiverBrowser;

    protected javax.jms.QueueBrowser senderBrowser;

    @org.junit.jupiter.api.Test
    public void testSenderBrowser() {
        try {
            javax.jms.TextMessage message_1 = senderSession.createTextMessage();
            message_1.setText("testBrowser:message_1");
            javax.jms.TextMessage message_2 = senderSession.createTextMessage();
            message_2.setText("testBrowser:message_2");
            sender.send(message_1);
            sender.send(message_2);
            java.util.Enumeration enumeration = senderBrowser.getEnumeration();
            int count = 0;
            while (enumeration.hasMoreElements()) {
                count++;
                java.lang.Object obj = enumeration.nextElement();
                junit.framework.TestCase.assertTrue((obj instanceof javax.jms.TextMessage));
                javax.jms.TextMessage msg = ((javax.jms.TextMessage) (obj));
                junit.framework.TestCase.assertTrue(msg.getText().startsWith("testBrowser:message_"));
            } 
            junit.framework.TestCase.assertEquals(2, count);
            javax.jms.Message m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue((m instanceof javax.jms.TextMessage));
            javax.jms.TextMessage msg = ((javax.jms.TextMessage) (m));
            junit.framework.TestCase.assertEquals("testBrowser:message_1", msg.getText());
            m = receiver.receive(jms.framework.TestConfig.TIMEOUT);
            junit.framework.TestCase.assertTrue((m instanceof javax.jms.TextMessage));
            msg = ((javax.jms.TextMessage) (m));
            junit.framework.TestCase.assertEquals("testBrowser:message_2", msg.getText());
            enumeration = receiverBrowser.getEnumeration();
            junit.framework.TestCase.assertTrue((!(enumeration.hasMoreElements())));
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    @org.junit.jupiter.api.Test
    public void testBrowserWithMessageSelector() {
        try {
            senderBrowser = senderSession.createBrowser(senderQueue, "pi = 3.14159");
            javax.jms.TextMessage message_1 = senderSession.createTextMessage();
            message_1.setText("testBrowserWithMessageSelector:message_1");
            javax.jms.TextMessage message_2 = senderSession.createTextMessage();
            message_2.setDoubleProperty("pi", 3.14159);
            message_2.setText("testBrowserWithMessageSelector:message_2");
            sender.send(message_1);
            sender.send(message_2);
            java.util.Enumeration enumeration = senderBrowser.getEnumeration();
            int count = 0;
            while (enumeration.hasMoreElements()) {
                count++;
                java.lang.Object obj = enumeration.nextElement();
                junit.framework.TestCase.assertTrue((obj instanceof javax.jms.TextMessage));
                javax.jms.TextMessage msg = ((javax.jms.TextMessage) (obj));
                junit.framework.TestCase.assertEquals("testBrowserWithMessageSelector:message_2", msg.getText());
            } 
            junit.framework.TestCase.assertEquals(1, count);
        } catch (javax.jms.JMSException e) {
            fail(e);
        }
    }

    public void setUp() {
        try {
            super.setUp();
            receiverBrowser = receiverSession.createBrowser(receiverQueue);
            senderBrowser = senderSession.createBrowser(senderQueue);
        } catch (javax.jms.JMSException e) {
            e.printStackTrace();
        }
    }

    public void tearDown() {
        try {
            receiverBrowser.close();
            senderBrowser.close();
            super.tearDown();
        } catch (javax.jms.JMSException e) {
            e.printStackTrace();
        } finally {
            receiverBrowser = null;
            senderBrowser = null;
        }
    }
}

