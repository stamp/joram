package org.ow2.joram.testutils;

import java.util.concurrent.ExecutorService;

import org.junit.jupiter.api.Test;

/**
 * Base class to adapt Botsing-generated tests to JUnit
 * Simply copy test0 method from Botsing test, annotate @Test, and fix imports
 * @author Pierre-Yves Gibello - OW2
 *
 */
public abstract class BotsingTestCase extends junit.framework.TestCase {

	protected ExecutorService executor = null;

	// Insert your test(s) method(s) here (eg. test0 ... testN)
	@Test
	abstract public void test0() throws Throwable;

	/**
	 * Replaces Botsing-generated verifyException()
	 * @param excString
	 * @param exc
	 */
	protected void verifyException(String excString, Exception exc) {
		assertTrue(exceptionContains(exc, excString));
	}

	private boolean exceptionContains(Exception exc, String value) {
		StackTraceElement[] stack = exc.getStackTrace();
		for(int i=0; i < stack.length; i++) {
			if(stack[i].toString().contains(value)) return true;
		}
		return false;
	}

	public void tearDown() {
        if(this.executor != null) {
        	 this.executor.shutdownNow();
        	 this.executor = null;
        }
    }
}
