package org.objectweb.joram.client.jms.admin;

import java.net.ConnectException;

import org.junit.jupiter.api.Test;

import jms.framework.PTPTestCase;

public class RestAcquisitionQueueTest extends PTPTestCase {

	@Test
	public void testCreate() {
		try {
			RestAcquisitionQueue queue = new RestAcquisitionQueue();
			assertNotNull(queue.create("RestAcquisitionQueue"));
			assertNotNull(queue.create(0, "RestAcquisitionQueue2"));
			
			assertEquals(queue.getHost(), "localhost");
			assertEquals(queue.getPort(), 8989);
			assertEquals(queue.getUsername(), "anonymous");
			assertEquals(queue.getPassword(), "anonymous");
			assertTrue(queue.isMediaTypeJson());
			assertEquals(queue.getTimeout(), 10000);
			assertEquals(queue.getAcquisitionPeriod(), -1);
			assertEquals(queue.getIdleTimeout(), 60);
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(AdminException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
