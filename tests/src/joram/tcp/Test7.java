/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2019 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package joram.tcp;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.objectweb.joram.client.jms.Queue;
import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;

import fr.dyade.aaa.agent.AgentServer;

/**
 * Test local connection with timeout (ConnectingTimer) and Transaction freezes.
 */
public class Test7 extends framework.TestCase {
  public static void main(String args[]) throws Exception {
    new Test7().run();
  }
  
  TcpConnectionFactory cf = null;
  Queue queue = null;
  
  public void run() {
    try {
      AgentServer.init((short) 0, "s0", null);
      AgentServer.start();
      Thread.sleep(1500L);
      
      cf = TcpConnectionFactory.create("localhost", 2560);
      cf.getParameters().connectingTimer = Integer.getInteger("connectingTimer", 0);
      System.out.println("connectingTimer=" + cf.getParameters().connectingTimer);
//      cf.getParameters().cnxPendingTimer = 1000;
//      cf.getParameters().SoTimeout = cf.getParameters().connectingTimer *1000;
//      cf.getParameters().SoLinger = 5000;
//      cf.getParameters().TcpNoDelay = true;
      AdminModule.connect(cf, "root", "root");
      User.create("anonymous", "anonymous", 0);
      queue = Queue.create(0);
      queue.setFreeReading();
      queue.setFreeWriting();
      AdminModule.disconnect();

      Exception expectedExc = null;
      Connection cnx = null;
      try {
        System.out.println("Should not throw an Exception.");
        cnx = cf.createConnection();
      } catch (Exception exc) {
        exc.printStackTrace();
        expectedExc = exc;
      }
      assertNotNull(cnx);
      assertNull(expectedExc);

      cnx.close();
      cnx = null;

      long timeToFreeze = Long.getLong("freeze", 30000L);
      System.out.println("freeze=" + timeToFreeze);
      new Thread() {
        public void run() {
          try {
            System.out.println("Freezes the server.");
            AgentServer.getTransaction().freeze(timeToFreeze);
          } catch (InterruptedException exc) {
            System.out.println("InterruptedException.");
          }
        }
      }.start();
      Thread.sleep(1000L);
      
      int nbcnx = Integer.getInteger("nbcnx", 10);
      for (int i=0; i<nbcnx; i++) {
        new CnxTest(i).start();
      }
      
      Thread.sleep(timeToFreeze);

      expectedExc = null;
      cnx = null;
      try {
        System.out.println("Should not throw an Exception.");
        cnx = cf.createConnection();
      } catch (Exception exc) {
        System.out.println("Throw an Exception: " + exc.getMessage());
        exc.printStackTrace();
        expectedExc = exc;
      }
      assertNotNull("Connection should be null", cnx);
      assertNull("Exception should not be null", expectedExc);

      Session sess = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageConsumer qconsumer = sess.createConsumer(queue);

      cnx.start();

      TextMessage msg;
      do {
        msg = (TextMessage) qconsumer.receive(1000L);
        assertNull("Message received", msg);
      } while (msg != null);

      cnx.close();
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      System.out.println("AgentServer.stop()");
      AgentServer.stop();
      endTest();
    }
  }

  class CnxTest extends Thread {
    private int rank;

    CnxTest(int rank) {
      this.rank = rank;
    }

    public void run() {
      Exception expectedExc = null;
      Connection cnx = null;
      long start = System.currentTimeMillis();
      try {
        System.out.println("#" + rank + "Should throw an Exception.");
        cnx = cf.createConnection();
        System.out.println("#" + rank + "Connection ok: " + (System.currentTimeMillis() - start) + "ms");
      } catch (Exception exc) {
        System.out.println("#" + rank + "Throw an Exception: " + (System.currentTimeMillis() - start) + "ms -> " + exc.getMessage());
        //        exc.printStackTrace();
        expectedExc = exc;
      }
      assertNull("Connection should be null", cnx);
      assertNotNull("Exception should not be null", expectedExc);

      if (cnx == null) return;

      try {
        Session sess1 = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
        MessageProducer producer = sess1.createProducer(null);

        cnx.start();

        TextMessage msg = sess1.createTextMessage();
        msg.setText("Test Queue with TCPConnectionFactory #" + rank);
        producer.send(queue, msg);
      } catch (JMSException exc) {
        exc.printStackTrace();
      }
    }
  }
}