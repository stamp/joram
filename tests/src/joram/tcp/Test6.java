/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C) 2019 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s): ScalAgent Distributed Technologies
 * Contributor(s): 
 */
package joram.tcp;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.objectweb.joram.client.jms.Queue;
import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;

import fr.dyade.aaa.agent.AgentServer;

/**
 * Test TCP connection with timeout (ConnectingTimer) and Transaction freezes.
 */
public class Test6 extends framework.TestCase {
  public static void main(String args[]) throws Exception {
    new Test6().run();
  }

  public void run() {
    try {
      AgentServer.init((short) 0, "s0", null);
      AgentServer.start();
      Thread.sleep(1500L);

      TcpConnectionFactory cf = TcpConnectionFactory.create("localhost", 2560);
      cf.getParameters().connectingTimer = 10;
      cf.getParameters().cnxPendingTimer = 5000;
      cf.getParameters().SoTimeout = 5000;
      cf.getParameters().SoLinger = 5000;
      cf.getParameters().TcpNoDelay = true;
      AdminModule.connect(cf, "root", "root");
      User.create("anonymous", "anonymous", 0);
      Queue queue = Queue.create(0);
      queue.setFreeReading();
      queue.setFreeWriting();
      AdminModule.disconnect();

      Exception expectedExc = null;
      Connection cnx = null;
      try {
        System.out.println("Should not throw an Exception.");
        cnx = cf.createConnection();
      } catch (Exception exc) {
        exc.printStackTrace();
        expectedExc = exc;
      }
      assertNotNull(cnx);
      assertNull(expectedExc);

      cnx.close();
      cnx = null;

      new Thread() {
        public void run() {
          try {
            AgentServer.getTransaction().freeze(30000L);
          } catch (InterruptedException exc) {
            System.out.println("InterruptedException.");
          }
        }
      }.start();
      Thread.sleep(1000L);

      expectedExc = null;
      cnx = null;
      long start = System.currentTimeMillis();
      try {
        System.out.println("Should throw an Exception.");
        cnx = cf.createConnection();
        System.out.println("Connection ok: " + (System.currentTimeMillis() - start) + "ms");
      } catch (Exception exc) {
        System.out.println("Throw an Exception: " + (System.currentTimeMillis() - start) + "ms -> " + exc.getMessage());
        //        exc.printStackTrace();
        expectedExc = exc;
      }
      assertNull(cnx);
      assertNotNull(expectedExc);

      Thread.sleep(20000L);

      expectedExc = null;
      cnx = null;
      try {
        System.out.println("Should not throw an Exception.");
        cnx = cf.createConnection();
      } catch (Exception exc) {
        System.out.println("Throw an Exception: " + exc.getMessage());
        exc.printStackTrace();
        expectedExc = exc;
      }
      assertNotNull(cnx);
      assertNull(expectedExc);

      Session sess1 = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageProducer producer = sess1.createProducer(null);

      Session sess2 = cnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageConsumer qconsumer = sess2.createConsumer(queue);

      cnx.start();

      TextMessage msg1 = sess1.createTextMessage();
      msg1.setText("Test Queue with LocalConnectionFactory");
      producer.send(queue, msg1);

      TextMessage msg2 = (TextMessage) qconsumer.receive(1000L);
      assertTrue(msg1.getText().equals(msg2.getText()));

      msg1 = sess1.createTextMessage();
      msg1.setText("Test Queue with LocalConnectionFactory");
      producer.send(queue, msg1);

      new Thread() {
        public void run() {
          try {
            AgentServer.getTransaction().freeze(5000L);
          } catch (InterruptedException exc) {}
        }
      }.start();
      Thread.sleep(100L);

      msg2 = null;
      try {
        start = System.currentTimeMillis();
        msg2 = (TextMessage) qconsumer.receive(1000L);
      } catch (JMSException exc) {
        System.out.println("Throw an Exception: " + exc.getMessage());
        exc.printStackTrace();
      }
      System.out.println("receive returns: " +  (System.currentTimeMillis() - start) + "ms");
      assertNull(msg2);

      cnx.close();
    } catch (Throwable exc) {
      exc.printStackTrace();
      error(exc);
    } finally {
      System.out.println("AgentServer.stop()");
      AgentServer.stop();
      endTest();
    }
  }
}
