/*
 * JORAM: Java(TM) Open Reliable Asynchronous Messaging
 * Copyright (C)  2019 ScalAgent Distributed Technologies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA.
 *
 * Initial developer(s):ScalAgent D.T.
 * Contributor(s): 
 */
package a3.base;

import fr.dyade.aaa.agent.*;
import framework.TestCase;

public class CheckServer extends TestCase {
  public CheckServer() {
    super();
  }

  protected void setUp() throws Exception {
    timeout = 100000L;
    AgentServer.registerCheckServerListener(new MyListener());
  }

  protected void startTest() throws Exception {
    AgentServer.start();
    setStartDate();
    DelayedAgent agent = new DelayedAgent((short) 0);
    agent.deploy();
    Channel.sendTo(agent.getId(), new DelayedNot(10000L));
    Thread.sleep(30000L);
    Channel.sendTo(agent.getId(), new DelayedNot(30000L));
    // Stops the test.
    Channel.sendTo(agent.getId(), new DelayedNot(-1L));
  }

  public static void main(String args[]) {
    new CheckServer().runTest(args);
  }

  static class MyListener implements CheckServerListener {
    public void onServerCheckError() {
      System.out.println("MyListener throwed");
    }
  }
  
  static class DelayedNot extends Notification {
    private static final long serialVersionUID = 1L;

    public long pause;

    public DelayedNot(long pause) {
      this.pause = pause;
    }
  }

  static class DelayedAgent extends Agent {
    private static final long serialVersionUID = 1L;

    public DelayedAgent(short to) {
      super(to, "Delayed#" + to);  
    }

    public void react(AgentId from, Notification not) {
      if (not instanceof DelayedNot) {
        long pause = ((DelayedNot) not).pause;
        if (pause > 0) {
          System.out.println("DelayedAgent: waits for " + pause);
          try {
            Thread.sleep(pause);
          } catch (InterruptedException exc) {}
          System.out.println("DelayedAgent: end waiting");
        } else {
          // Stops the test
          TestCase.endTest();
        }
      }
    }
  }
}
