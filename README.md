# STAMP OW2 experiments on Joram

This project shows how Joram could be enhanced thanks to STAMP project:
- Descartes analysis, then new tests added to increase mutation and line coverage (non-automated guided development).
- A first pass of amplification was done using DSpot (automated mode).
- A second pass of amplification was done using Botsing, with 2 steps: Botsing and Model seeding (automated mode, then ported to JUnit).

Each step above corresponds to a commit in this project, that also contains Descartes measures to show the progress at each step.

Details of STAMP experiments outputs and results in the [project wiki](https://gitlab.ow2.org/stamp/joram/wikis/OW2-STAMP-experiments-on-Joram-project-(Descartes,-DSpot,-Botsing,-model-seeding)).

To build the project (in joram/ sub-directory):
```
mvn clean install -DskipTests
```

The 1st build requires tests to be skipped, because generating a Joram distribution is necessary to run tests (many tests being integration tests).

Then, to run tests with STAMP amplification:

```
mvn test
```

and to run Descartes for mutation and line coverage measurements:

```
mvn eu.stamp-project:pitmp-maven-plugin:descartes
```
